#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

#include <linux/elf.h>

#define PRINT_ERRNO (printf("%d, %s\n", (errno), strerror((errno))))

int readFile(const char* path, char** buffer)
{
	int fd = -1;
	int ret = -1;
	int fileSize = 0;
	int numBytes = 0;
	int totalBytes = 0;
	struct stat st;

	if ((fd = open(path, O_RDONLY)) < 0)
	{
		PRINT_ERRNO;
		goto exit;
	}
	if (stat(path, &st) < 0)
	{
		PRINT_ERRNO;
		goto exit;
	}

	fileSize = st.st_size;
	*buffer = (char*)malloc(fileSize);

	printf("File %s is %d bytes.\n", path, fileSize);

	if (!(*buffer) && fileSize > 0)
	{
		printf("Failed to allocate memory\n");
		buffer = NULL;
	}


	while (totalBytes < fileSize)
	{
		numBytes = read(fd, *buffer + numBytes, fileSize);
		if (numBytes < 0)
		{
			PRINT_ERRNO;
			free(*buffer);
			*buffer = NULL;
			goto exit;
		}
		totalBytes += numBytes;
	}

	if (totalBytes == fileSize)
	{	
		ret = 0;
	}

exit:

	return ret;
}


int main(int argc, char* argv[])
{
	const char* fileName = argv[1];
	char* buffer = NULL;
	int numBytes = readFile(fileName, &buffer);
	struct elf64_hdr *header;

	if (numBytes < 0)
	{
		printf("Failed to read file %s.\n", fileName);
		return -1;
	}

	header = (struct elf64_hdr*)buffer;
	if (strncmp(header->e_ident, ELFMAG, strlen(ELFMAG)) != 0)
	{
		printf("Not a valid ELF file!\n");
		return -1;
	}
	
	return 0;
}
